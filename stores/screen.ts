type ScreenStepType = 'LOGIN' | 'DESKTOP'
export const useScreenStore = defineStore('screen', {
    state: () => ({ 
        step: 'LOGIN' as ScreenStepType
    }),
    getters: {
        getCurrentScreen: (state) => state.step
    },
    actions: {
        setCurrentScreen(step: ScreenStepType){
            this.step = step
        }
    },
    persist: {
        storage: persistedState.sessionStorage,
    },
  })