// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@nuxt/ui', '@pinia-plugin-persistedstate/nuxt', '@pinia/nuxt'],
  css: [
    '~/assets/scss/main.scss',
    '~/assets/scss/responsive.scss'
  ],
  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1'
    }
  }
})
